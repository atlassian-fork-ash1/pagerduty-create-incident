# Bitbucket Pipelines Pipe: PagerDuty Create Incident

Creates a [PagerDuty](https://pagerduty.com) incident.

Uses the synchronous [Incident Creation API REST APIv2](https://v2.developer.pagerduty.com/docs/incident-creation-api).

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: atlassian/pagerduty-create-incident:0.2.4
  variables:
    API_KEY:  '<string>'
    EMAIL:  '<string>'
    SERVICE_ID:  '<string>'
    # URGENCY: '<string>' # Optional
    # INCIDENT_TITLE: '<string>' # Optional
    # CLIENT_URL: '<string>' # Optional
    # DESCRIPTION: '<string>' # Optional
    # DEBUG: '<string>' # Optional
```
## Variables

| Variable              | Usage                                                       |
| --------------------- | ----------------------------------------------------------- |
| API_KEY (*)           | The API Access Keys for API authorization. |
| EMAIL (*)             | The email address of a valid PagerDuty user on the account associated with the API key. |
| SERVICE_ID (*)        | ID of the service having an incident.|
| URGENCY               | Urgency of the incident. Default: `high`. Can be `high` or `low` only.|
| INCIDENT_TITLE           | Title of the incident. Default: `Incident triggered from Bitbucket Pipelines`.|
| CLIENT_URL            | URL of the sending entity, default to the pipeline URL <https://bitbucket.org/${BITBUCKET_WORKSPACE}/${BITBUCKET_REPO_SLUG}/addon/pipelines/home#!/results/${BITBUCKET_BUILD_NUMBER}>.|
| DESCRIPTION           | Short description of the incident. Default: `Incident triggered from Bitbucket Pipelines {CLIENT_URL}`.|
| DEBUG                 | Turn on extra debug information. Default: `false`. |

_(*) = required variable._

## Prerequisites

To create incident PagerDuty, you'll need an API key. You can follow the instructions [here](https://support.pagerduty.com/docs/using-the-api#section-generating-a-general-access-rest-api-key) to create one.

To find the service ID you should go to *Configuration -> Services* in PagerDuty's main menu.

## Examples

Basic example:

```yaml
script:
  - pipe: atlassian/pagerduty-create-incident:0.2.4
    variables:
      API_KEY: $API_KEY
      EMAIL: $EMAIL
      SERVICE_ID: $SERVICE_ID
```

Advanced example:

```yaml
script:
  - pipe: atlassian/pagerduty-create-incident:0.2.4
    variables:
      API_KEY: $API_KEY
      EMAIL: $EMAIL
      SERVICE_ID: $SERVICE_ID
      URGENCY: 'low'
      INCIDENT_TITLE: 'Incident triggered from Bitbucket Pipelines'
      DESCRIPTION: 'Incident happened during execution Bitbucket Pipelines.'
```

## Support
If you’d like help with this pipe, or you have an issue or feature request, [let us know on Community][community].

If you’re reporting an issue, please include:

- the version of the pipe
- relevant logs and error messages
- steps to reproduce


[community]: https://community.atlassian.com/t5/forums/postpage/board-id/bitbucket-pipelines-questions?add-tags=pipes,notify,pagerduty,incident
